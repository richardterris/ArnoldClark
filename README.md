# ArnoldClark
Technical Test for Arnold Clark

# Notes
* Agreement Fee, Completion Fee, and Minimum Deposit can be edited in the appsettings.json file
* It's likely that in a real application, a javascript framework such as React or Angular would be used, but for this simple 
example I used ajax
* In a real application I would have a dto object so as not to pass the post model object into the service layer
