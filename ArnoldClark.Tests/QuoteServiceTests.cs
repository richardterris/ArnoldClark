using ArnoldClark.Models;
using ArnoldClark.Services;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using ArnoldClark.ExtensionMethods;

namespace Tests
{
    public class QuoteServiceTests
    {
        Mock<IQuoteService> mockQuoteService;
        QuotePostModel quotePostModel;
        Mock<PaymentSchedule> paymentSchedule;
        List<KeyValuePair<DateTime, Installment>> installmentsOneYear;
        List<KeyValuePair<DateTime, Installment>> installmentsTwoYears;
        List<KeyValuePair<DateTime, Installment>> installmentsThreeYears;

        [SetUp]
        public void Setup()
        {
            mockQuoteService = new Mock<IQuoteService>();
            quotePostModel = new QuotePostModel
            {
                DeliveryDate = new DateTime(2019, 09, 01),
                Deposit = 4000,
                VehiclePrice = 20000
            };

            paymentSchedule = new Mock<PaymentSchedule>();

            installmentsOneYear = new List<KeyValuePair<DateTime, Installment>>
            {
               new KeyValuePair<DateTime, Installment>(new DateTime(2019, 10, 7), new Installment{ InstallmentAmount = 1421.33m, AmountOutstanding = 14686.67m }),
               new KeyValuePair<DateTime, Installment>(new DateTime(2019, 11, 4), new Installment{ InstallmentAmount = 1333.33m, AmountOutstanding = 13353.33m }),
               new KeyValuePair<DateTime, Installment>(new DateTime(2019, 12, 2), new Installment{ InstallmentAmount = 1333.33m, AmountOutstanding = 12020.00m }),
               new KeyValuePair<DateTime, Installment>(new DateTime(2020, 1, 6), new Installment{ InstallmentAmount = 1333.33m, AmountOutstanding = 10686.67m }),
               new KeyValuePair<DateTime, Installment>(new DateTime(2020, 2, 3), new Installment{ InstallmentAmount = 1333.33m, AmountOutstanding = 9353.33m }),
               new KeyValuePair<DateTime, Installment>(new DateTime(2020, 3, 2), new Installment{ InstallmentAmount = 1333.33m, AmountOutstanding = 8020.00m }),
               new KeyValuePair<DateTime, Installment>(new DateTime(2020, 4, 6), new Installment{ InstallmentAmount = 1333.33m, AmountOutstanding = 6686.67m }),
               new KeyValuePair<DateTime, Installment>(new DateTime(2020, 5, 4), new Installment{ InstallmentAmount = 1333.33m, AmountOutstanding = 5353.33m }),
               new KeyValuePair<DateTime, Installment>(new DateTime(2020, 6, 1), new Installment{ InstallmentAmount = 1333.33m, AmountOutstanding = 4020.00m }),
               new KeyValuePair<DateTime, Installment>(new DateTime(2020, 7,6), new Installment{ InstallmentAmount = 1333.33m, AmountOutstanding = 2686.67m }),
               new KeyValuePair<DateTime, Installment>(new DateTime(2020, 8, 3), new Installment{ InstallmentAmount = 1333.33m, AmountOutstanding = 1353.33m }),
               new KeyValuePair<DateTime, Installment>(new DateTime(2020, 9, 7), new Installment{ InstallmentAmount = 1353.33m, AmountOutstanding = 0.00m })
            };

            installmentsTwoYears = new List<KeyValuePair<DateTime, Installment>>
            {
               new KeyValuePair<DateTime, Installment>(new DateTime(2019, 10, 7), new Installment{ InstallmentAmount = 754.66m, AmountOutstanding = 0 }),
               new KeyValuePair<DateTime, Installment>(new DateTime(2019, 11, 4), new Installment{ InstallmentAmount = 666.66m, AmountOutstanding = 0 }),
               new KeyValuePair<DateTime, Installment>(new DateTime(2019, 12, 2), new Installment{ InstallmentAmount = 666.66m, AmountOutstanding = 0 }),
               new KeyValuePair<DateTime, Installment>(new DateTime(2020, 1, 6), new Installment{ InstallmentAmount = 666.66m, AmountOutstanding = 0 }),
               new KeyValuePair<DateTime, Installment>(new DateTime(2020, 2, 3), new Installment{ InstallmentAmount = 666.66m, AmountOutstanding = 0 }),
               new KeyValuePair<DateTime, Installment>(new DateTime(2020, 3, 2), new Installment{ InstallmentAmount = 666.66m, AmountOutstanding = 0 }),
               new KeyValuePair<DateTime, Installment>(new DateTime(2020, 4, 6), new Installment{ InstallmentAmount = 666.66m, AmountOutstanding = 0 }),
               new KeyValuePair<DateTime, Installment>(new DateTime(2020, 5, 4), new Installment{ InstallmentAmount = 666.66m, AmountOutstanding = 0 }),
               new KeyValuePair<DateTime, Installment>(new DateTime(2020, 6, 1), new Installment{ InstallmentAmount = 666.66m, AmountOutstanding = 0 }),
               new KeyValuePair<DateTime, Installment>(new DateTime(2020, 7,6), new Installment{ InstallmentAmount = 666.66m, AmountOutstanding = 0 }),
               new KeyValuePair<DateTime, Installment>(new DateTime(2020, 8, 3), new Installment{ InstallmentAmount = 666.66m, AmountOutstanding = 0 }),
               new KeyValuePair<DateTime, Installment>(new DateTime(2020, 9, 7), new Installment{ InstallmentAmount = 666.66m, AmountOutstanding = 0 }),
               new KeyValuePair<DateTime, Installment>(new DateTime(2020, 10, 5), new Installment{ InstallmentAmount = 666.66m, AmountOutstanding = 0 }),
               new KeyValuePair<DateTime, Installment>(new DateTime(2020, 11, 2), new Installment{ InstallmentAmount = 666.66m, AmountOutstanding = 0 }),
               new KeyValuePair<DateTime, Installment>(new DateTime(2020, 12, 7), new Installment{ InstallmentAmount = 666.66m, AmountOutstanding = 0 }),
               new KeyValuePair<DateTime, Installment>(new DateTime(2021, 1, 4), new Installment{ InstallmentAmount = 666.66m, AmountOutstanding = 0 }),
               new KeyValuePair<DateTime, Installment>(new DateTime(2021, 2, 1), new Installment{ InstallmentAmount = 666.66m, AmountOutstanding = 0 }),
               new KeyValuePair<DateTime, Installment>(new DateTime(2021, 3, 1), new Installment{ InstallmentAmount = 666.66m, AmountOutstanding = 0 }),
               new KeyValuePair<DateTime, Installment>(new DateTime(2021, 4, 5), new Installment{ InstallmentAmount = 666.66m, AmountOutstanding = 0 }),
               new KeyValuePair<DateTime, Installment>(new DateTime(2021, 5, 3), new Installment{ InstallmentAmount = 666.66m, AmountOutstanding = 0 }),
               new KeyValuePair<DateTime, Installment>(new DateTime(2021, 6, 7), new Installment{ InstallmentAmount = 666.66m, AmountOutstanding = 0 }),
               new KeyValuePair<DateTime, Installment>(new DateTime(2021, 7, 5), new Installment{ InstallmentAmount = 666.66m, AmountOutstanding = 0 }),
               new KeyValuePair<DateTime, Installment>(new DateTime(2021, 8, 2), new Installment{ InstallmentAmount = 666.66m, AmountOutstanding = 0 }),
               new KeyValuePair<DateTime, Installment>(new DateTime(2021, 9, 6), new Installment{ InstallmentAmount = 686.66m, AmountOutstanding = 0 })
            };

            installmentsThreeYears = new List<KeyValuePair<DateTime, Installment>>
            {
               new KeyValuePair<DateTime, Installment>(new DateTime(2019, 10, 7), new Installment{ InstallmentAmount = 532.44m, AmountOutstanding = 0 }),
               new KeyValuePair<DateTime, Installment>(new DateTime(2019, 11, 4), new Installment{ InstallmentAmount = 444.44m, AmountOutstanding = 0 }),
               new KeyValuePair<DateTime, Installment>(new DateTime(2019, 12, 2), new Installment{ InstallmentAmount = 444.44m, AmountOutstanding = 0 }),
               new KeyValuePair<DateTime, Installment>(new DateTime(2020, 1, 6), new Installment{ InstallmentAmount = 444.44m, AmountOutstanding = 0 }),
               new KeyValuePair<DateTime, Installment>(new DateTime(2020, 2, 3), new Installment{ InstallmentAmount = 444.44m, AmountOutstanding = 0 }),
               new KeyValuePair<DateTime, Installment>(new DateTime(2020, 3, 2), new Installment{ InstallmentAmount = 444.44m, AmountOutstanding = 0 }),
               new KeyValuePair<DateTime, Installment>(new DateTime(2020, 4, 6), new Installment{ InstallmentAmount = 444.44m, AmountOutstanding = 0 }),
               new KeyValuePair<DateTime, Installment>(new DateTime(2020, 5, 4), new Installment{ InstallmentAmount = 444.44m, AmountOutstanding = 0 }),
               new KeyValuePair<DateTime, Installment>(new DateTime(2020, 6, 1), new Installment{ InstallmentAmount = 444.44m, AmountOutstanding = 0 }),
               new KeyValuePair<DateTime, Installment>(new DateTime(2020, 7,6), new Installment{ InstallmentAmount = 444.44m, AmountOutstanding = 0 }),
               new KeyValuePair<DateTime, Installment>(new DateTime(2020, 8, 3), new Installment{ InstallmentAmount = 444.44m, AmountOutstanding = 0 }),
               new KeyValuePair<DateTime, Installment>(new DateTime(2020, 9, 7), new Installment{ InstallmentAmount = 444.44m, AmountOutstanding = 0 }),

               new KeyValuePair<DateTime, Installment>(new DateTime(2020, 10, 5), new Installment{ InstallmentAmount = 444.44m, AmountOutstanding = 0 }),
               new KeyValuePair<DateTime, Installment>(new DateTime(2020, 11, 2), new Installment{ InstallmentAmount = 444.44m, AmountOutstanding = 0 }),
               new KeyValuePair<DateTime, Installment>(new DateTime(2020, 12, 7), new Installment{ InstallmentAmount = 444.44m, AmountOutstanding = 0 }),
               new KeyValuePair<DateTime, Installment>(new DateTime(2021, 1, 4), new Installment{ InstallmentAmount = 444.44m, AmountOutstanding = 0 }),
               new KeyValuePair<DateTime, Installment>(new DateTime(2021, 2, 1), new Installment{ InstallmentAmount = 444.44m, AmountOutstanding = 0 }),
               new KeyValuePair<DateTime, Installment>(new DateTime(2021, 3, 1), new Installment{ InstallmentAmount = 444.44m, AmountOutstanding = 0 }),
               new KeyValuePair<DateTime, Installment>(new DateTime(2021, 4, 5), new Installment{ InstallmentAmount = 444.44m, AmountOutstanding = 0 }),
               new KeyValuePair<DateTime, Installment>(new DateTime(2021, 5, 3), new Installment{ InstallmentAmount = 444.44m, AmountOutstanding = 0 }),
               new KeyValuePair<DateTime, Installment>(new DateTime(2021, 6, 7), new Installment{ InstallmentAmount = 444.44m, AmountOutstanding = 0 }),
               new KeyValuePair<DateTime, Installment>(new DateTime(2021, 7, 5), new Installment{ InstallmentAmount = 444.44m, AmountOutstanding = 0 }),
               new KeyValuePair<DateTime, Installment>(new DateTime(2021, 8, 2), new Installment{ InstallmentAmount = 444.44m, AmountOutstanding = 0 }),
               new KeyValuePair<DateTime, Installment>(new DateTime(2021, 9, 6), new Installment{ InstallmentAmount = 444.44m, AmountOutstanding = 0 }),

               new KeyValuePair<DateTime, Installment>(new DateTime(2021, 10, 4), new Installment{ InstallmentAmount = 444.44m, AmountOutstanding = 0 }),
               new KeyValuePair<DateTime, Installment>(new DateTime(2021, 11, 8), new Installment{ InstallmentAmount = 444.44m, AmountOutstanding = 0 }),
               new KeyValuePair<DateTime, Installment>(new DateTime(2021, 12, 6), new Installment{ InstallmentAmount = 444.44m, AmountOutstanding = 0 }),
               new KeyValuePair<DateTime, Installment>(new DateTime(2022, 1, 3), new Installment{ InstallmentAmount = 444.44m, AmountOutstanding = 0 }),
               new KeyValuePair<DateTime, Installment>(new DateTime(2022, 2, 7), new Installment{ InstallmentAmount = 444.44m, AmountOutstanding = 0 }),
               new KeyValuePair<DateTime, Installment>(new DateTime(2022, 3, 7), new Installment{ InstallmentAmount = 444.44m, AmountOutstanding = 0 }),
               new KeyValuePair<DateTime, Installment>(new DateTime(2022, 4, 4), new Installment{ InstallmentAmount = 444.44m, AmountOutstanding = 0 }),
               new KeyValuePair<DateTime, Installment>(new DateTime(2022, 5, 9), new Installment{ InstallmentAmount = 444.44m, AmountOutstanding = 0 }),
               new KeyValuePair<DateTime, Installment>(new DateTime(2022, 6, 6), new Installment{ InstallmentAmount = 444.44m, AmountOutstanding = 0 }),
               new KeyValuePair<DateTime, Installment>(new DateTime(2022, 7, 4), new Installment{ InstallmentAmount = 444.44m, AmountOutstanding = 0 }),
               new KeyValuePair<DateTime, Installment>(new DateTime(2022, 8, 1), new Installment{ InstallmentAmount = 444.44m, AmountOutstanding = 0 }),
               new KeyValuePair<DateTime, Installment>(new DateTime(2022, 9, 5), new Installment{ InstallmentAmount = 464.44m, AmountOutstanding = 0 })
            };
        }

        [Test]
        public void Return_First_Monday_Of_Month()
        {
            DateTime startDate = new DateTime(2019, 06, 01);

            Assert.AreEqual(new DateTime(2019, 06, 03), startDate.GetFirstMondayOfMonth().Date);
        }

        [Test]
        public void Correct_Loan_Amount_Remaining()
        {
            //Arrange
            decimal amountRemainingAfter6Payments = 7912.02m;
            quotePostModel.LoanTerm = LoanTermEnum.One;
            int loanTerm = (int)quotePostModel.LoanTerm;
            //Act
            mockQuoteService.Setup(x => x.CalculateAmountRemaining(6, quotePostModel.VehiclePrice, loanTerm, quotePostModel.Deposit)).Returns(7912.02m);

            var remain = mockQuoteService.Object.CalculateAmountRemaining(6, quotePostModel.VehiclePrice, loanTerm, quotePostModel.Deposit);

            //Assert
            Assert.AreEqual(amountRemainingAfter6Payments, remain);
        }

        [Test]
        public void Minimum_Deposit_Provided()
        {
            //Arrange
            decimal minimumDeposit = (quotePostModel.VehiclePrice / 100) * 15;

            Assert.GreaterOrEqual(quotePostModel.Deposit, minimumDeposit);
        }

        [Test]
        public void Minimum_Deposit_Not_Provided()
        {
            //Arrange
            decimal minimumDeposit = (quotePostModel.VehiclePrice / 100) * 15;
            quotePostModel.Deposit = 2000;

            Assert.LessOrEqual(quotePostModel.Deposit, minimumDeposit);

        }

        [Test]
        public void Schedule_For_One_Year_Correct_Installments()
        {
            //Arrange
            quotePostModel.LoanTerm = LoanTermEnum.One;
            paymentSchedule.Object.Installments = installmentsOneYear;
            mockQuoteService.Setup(x => x.GetPaymentSchedule(quotePostModel)).Returns(() => paymentSchedule.Object);
            mockQuoteService.Object.GetPaymentSchedule(quotePostModel);

            //Act
            var installmentsOutsideFirstAndLast = paymentSchedule.Object.Installments.Skip(1).SkipLast(1);

            //Assert
            Assert.AreEqual(12, paymentSchedule.Object.Installments.Count);
            Assert.AreEqual(new decimal(1421.33), paymentSchedule.Object.Installments.First().Value.InstallmentAmount);
            foreach (var installment in installmentsOutsideFirstAndLast)
            {
                Assert.AreEqual(new decimal(1333.33), installment.Value.InstallmentAmount);
            }
            Assert.AreEqual(new decimal(1353.33), paymentSchedule.Object.Installments.Last().Value.InstallmentAmount);
        }

        [Test]
        public void Schedule_For_One_Year_InCorrect_Installments()
        {
            //Arrange
            quotePostModel.LoanTerm = LoanTermEnum.One;
            paymentSchedule.Object.Installments = installmentsOneYear;
            mockQuoteService.Setup(x => x.GetPaymentSchedule(quotePostModel)).Returns(() => paymentSchedule.Object);
            mockQuoteService.Object.GetPaymentSchedule(quotePostModel);

            //Act
            var installmentsOutsideFirstAndLast = paymentSchedule.Object.Installments.Skip(1).SkipLast(1);

            //Assert
            Assert.AreNotEqual(13, paymentSchedule.Object.Installments.Count);
            Assert.AreNotEqual(new decimal(1455.33), paymentSchedule.Object.Installments.First().Value.InstallmentAmount);
            foreach (var installment in installmentsOutsideFirstAndLast)
            {
                Assert.AreNotEqual(new decimal(1355.33), installment.Value.InstallmentAmount);
            }
            Assert.AreNotEqual(new decimal(1366.33), paymentSchedule.Object.Installments.Last().Value);
        }

        [Test]
        public void Schedule_For_Two_Years_Correct_Installments()
        {
            //Arrange
            quotePostModel.LoanTerm = LoanTermEnum.Two;
            paymentSchedule.Object.Installments = installmentsTwoYears;
            mockQuoteService.Setup(x => x.GetPaymentSchedule(quotePostModel)).Returns(() => paymentSchedule.Object);
            mockQuoteService.Object.GetPaymentSchedule(quotePostModel);

            //Act
            var installmentsOutsideFirstAndLast = paymentSchedule.Object.Installments.Skip(1).SkipLast(1);

            //Assert
            Assert.AreEqual(24, paymentSchedule.Object.Installments.Count);
            Assert.AreEqual(new decimal(754.66), paymentSchedule.Object.Installments.First().Value.InstallmentAmount);
            foreach (var installment in installmentsOutsideFirstAndLast)
            {
                Assert.AreEqual(new decimal(666.66), installment.Value.InstallmentAmount);
            }
            Assert.AreEqual(new decimal(686.66), paymentSchedule.Object.Installments.Last().Value.InstallmentAmount);
        }

        [Test]
        public void Schedule_For_Two_Years_InCorrect_Installments()
        {
            //Arrange
            quotePostModel.LoanTerm = LoanTermEnum.Two;
            paymentSchedule.Object.Installments = installmentsTwoYears;
            mockQuoteService.Setup(x => x.GetPaymentSchedule(quotePostModel)).Returns(() => paymentSchedule.Object);
            mockQuoteService.Object.GetPaymentSchedule(quotePostModel);

            //Act
            var installmentsOutsideFirstAndLast = paymentSchedule.Object.Installments.Skip(1).SkipLast(1);

            //Assert
            Assert.AreNotEqual(25, paymentSchedule.Object.Installments.Count);
            Assert.AreNotEqual(new decimal(777.67), paymentSchedule.Object.Installments.First().Value.InstallmentAmount);
            foreach (var installment in installmentsOutsideFirstAndLast)
            {
                Assert.AreNotEqual(new decimal(677.67), installment.Value.InstallmentAmount);
            }
            Assert.AreNotEqual(new decimal(677.67), paymentSchedule.Object.Installments.Last().Value.InstallmentAmount);
        }

        [Test]
        public void Schedule_For_Three_Years_Correct_Installments()
        {
            //Arrange
            quotePostModel.LoanTerm = LoanTermEnum.Three;
            paymentSchedule.Object.Installments = installmentsThreeYears;
            mockQuoteService.Setup(x => x.GetPaymentSchedule(quotePostModel)).Returns(() => paymentSchedule.Object);
            mockQuoteService.Object.GetPaymentSchedule(quotePostModel);

            //Act
            var installmentsOutsideFirstAndLast = paymentSchedule.Object.Installments.Skip(1).SkipLast(1);

            //Assert
            Assert.AreEqual(36, paymentSchedule.Object.Installments.Count);
            Assert.AreEqual(new decimal(532.44), paymentSchedule.Object.Installments.First().Value.InstallmentAmount);
            foreach (var installment in installmentsOutsideFirstAndLast)
            {
                Assert.AreEqual(new decimal(444.44), installment.Value.InstallmentAmount);
            }
            Assert.AreEqual(new decimal(464.44), paymentSchedule.Object.Installments.Last().Value.InstallmentAmount);
        }

        [Test]
        public void Schedule_For_Three_Years_InCorrect_Installments()
        {
            //Arrange
            quotePostModel.LoanTerm = LoanTermEnum.Three;
            paymentSchedule.Object.Installments = installmentsThreeYears;
            mockQuoteService.Setup(x => x.GetPaymentSchedule(quotePostModel)).Returns(() => paymentSchedule.Object);
            mockQuoteService.Object.GetPaymentSchedule(quotePostModel);

            //Act
            var installmentsOutsideFirstAndLast = paymentSchedule.Object.Installments.Skip(1).SkipLast(1);

            //Assert
            Assert.AreNotEqual(39, paymentSchedule.Object.Installments.Count);
            Assert.AreNotEqual(new decimal(555.44), paymentSchedule.Object.Installments.First().Value.InstallmentAmount);
            foreach (var installment in installmentsOutsideFirstAndLast)
            {
                Assert.AreNotEqual(new decimal(455.44), installment.Value.InstallmentAmount);
            }
            Assert.AreNotEqual(new decimal(455.44), paymentSchedule.Object.Installments.Last().Value.InstallmentAmount);
        }
    }
}