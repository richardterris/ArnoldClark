﻿using ArnoldClark.Config;
using ArnoldClark.Models;
using ArnoldClark.Services;
using ArnoldClark.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using System;

namespace ArnoldClark.Controllers
{
    public class QuoteController : Controller
    {
        private readonly Fees _fees;

        public QuoteController(IOptions<Fees> fees)
        {
            _fees = fees.Value;
        }

        [HttpPost]
        public IActionResult GetQuote([FromServices] IQuoteService quoteService,[FromBody] QuotePostModel quotePostModel)
        {
            decimal minimumDeposit = Math.Round(_fees.MinimumDeposit > 0 ? _fees.MinimumDeposit : 15, 2, MidpointRounding.AwayFromZero);
            if(quotePostModel.Deposit < ((quotePostModel.VehiclePrice/100) * minimumDeposit))
            {
                return PartialView("MinimumDeposit");
            }

            LoanQuoteViewModel loanQuoteViewModel = new LoanQuoteViewModel
            {
                Schedule = quoteService.GetPaymentSchedule(quotePostModel),
                VehiclePrice = quotePostModel.VehiclePrice,
                DepositAmount = quotePostModel.Deposit,
                DeliveryDate = quotePostModel.DeliveryDate,
                LoanTerm = (int)quotePostModel.LoanTerm
            };

            return PartialView("QuoteSummary", loanQuoteViewModel);
        }
    }
}