﻿using ArnoldClark.Models;
using System;
using System.ComponentModel.DataAnnotations;

namespace ArnoldClark.ViewModels
{
    public class LoanQuoteViewModel
    {
        [UIHint("Currency")]
        public decimal VehiclePrice { get; set; }
        [UIHint("Currency")]
        public decimal DepositAmount { get; set; }
        [UIHint("Date")]
        public DateTime DeliveryDate { get; set; }
        public int LoanTerm { get; set; }
        public PaymentSchedule Schedule { get; set; }
    }
}
