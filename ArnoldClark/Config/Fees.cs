﻿namespace ArnoldClark.Config
{
    public class Fees
    {
        public decimal AgreementFee { get; set; }
        public decimal CompletionFee { get; set; }
        public decimal MinimumDeposit { get; set; }
    }
}
