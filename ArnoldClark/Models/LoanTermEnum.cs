﻿using System.ComponentModel.DataAnnotations;

namespace ArnoldClark.Models
{
    public enum LoanTermEnum
    {
        [Display(Name ="1")]
        One= 12,
        [Display(Name ="2")]
        Two = 24,
        [Display(Name = "3")]
        Three = 36
    }
}
