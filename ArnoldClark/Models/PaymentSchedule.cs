﻿using System;
using System.Collections.Generic;

namespace ArnoldClark.Models
{
    public class PaymentSchedule
    {
        public List<KeyValuePair<DateTime, Installment>> Installments { get; set; }
    }
}
