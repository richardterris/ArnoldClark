﻿namespace ArnoldClark.Models
{
    public class Installment
    {
        public decimal InstallmentAmount { get; set; }
        public decimal AmountOutstanding { get; set; }
    }
}
