﻿using System;

namespace ArnoldClark.Models
{
    public class QuotePostModel
    {
        public decimal VehiclePrice { get; set; }
        public decimal Deposit { get; set; }
        public DateTime DeliveryDate { get; set; }
        public LoanTermEnum LoanTerm { get; set; }
    }
}
