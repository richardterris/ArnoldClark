﻿using System;

namespace ArnoldClark.ExtensionMethods
{
    public static class DateExtensionMethods
    {
        public static DateTime GetFirstMondayOfMonth(this DateTime startDate)
        {
            switch(startDate.DayOfWeek)
            {
                case DayOfWeek.Monday:
                    return startDate;
                case DayOfWeek.Tuesday:
                    return startDate.AddDays(6);
                case DayOfWeek.Wednesday:
                    return startDate.AddDays(5);
                case DayOfWeek.Thursday:
                    return startDate.AddDays(4);
                case DayOfWeek.Friday:
                    return startDate.AddDays(3);
                case DayOfWeek.Saturday:
                    return startDate.AddDays(2);
                case DayOfWeek.Sunday:
                    return startDate.AddDays(1);
                default:
                    return startDate;
            }
        }
    }
}
