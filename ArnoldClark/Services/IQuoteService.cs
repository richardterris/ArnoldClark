﻿using ArnoldClark.Models;

namespace ArnoldClark.Services
{
    public interface IQuoteService
    {
        PaymentSchedule GetPaymentSchedule(QuotePostModel quotePostModel);
        decimal CalculateAmountRemaining(int installmentNumber, decimal vehiclePrice, int loanTermMonths, decimal deposit);
    }
}
