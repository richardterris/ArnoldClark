﻿using ArnoldClark.Config;
using ArnoldClark.Models;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using ArnoldClark.ExtensionMethods;

namespace ArnoldClark.Services
{
    public class QuoteService : IQuoteService
    {
        private readonly PaymentSchedule _paymentSchedule;
        private decimal _installmentAmount;
        private readonly Fees _fees;

        public QuoteService(IOptions<Fees> fees)
        {
            _paymentSchedule = new PaymentSchedule();
            _fees = fees.Value;
        }

        /// <summary>
        /// Get the payment schedule
        /// </summary>
        /// <param name="quotePostModel"></param>
        /// <returns></returns>
        public PaymentSchedule GetPaymentSchedule(QuotePostModel quotePostModel)
        {
            _installmentAmount = Math.Round((quotePostModel.VehiclePrice - quotePostModel.Deposit) / (int)quotePostModel.LoanTerm, 2, MidpointRounding.AwayFromZero);
            List<KeyValuePair<DateTime, Installment>> installments = new List<KeyValuePair<DateTime, Installment>>();

            DateTime loanStartDate = new DateTime(quotePostModel.DeliveryDate.Year, quotePostModel.DeliveryDate.Month + 1, 1);
            for (int month = 0; month < (int)quotePostModel.LoanTerm; month++)
            {
                DateTime dateTime = loanStartDate.AddMonths(month);
                var installment = new Installment
                {
                    InstallmentAmount = Math.Round(_installmentAmount, 2, MidpointRounding.AwayFromZero),
                    AmountOutstanding = CalculateAmountRemaining(month + 1, quotePostModel.VehiclePrice, (int)quotePostModel.LoanTerm, quotePostModel.Deposit)
                };

                installments.Add(new KeyValuePair<DateTime, Installment>(dateTime.GetFirstMondayOfMonth(), installment));
            }

            AddAgreementFee(installments);
            AddCompletionFee(installments);

            _paymentSchedule.Installments = installments;

            return _paymentSchedule;

        }

        /// <summary>
        /// Calculate the amount remaining at the time of this installment
        /// </summary>
        /// <param name="installmentNumber"></param>
        /// <param name="vehiclePrice"></param>
        /// <param name="loanTermMonths"></param>
        /// <param name="deposit"></param>
        /// <returns></returns>
        public decimal CalculateAmountRemaining(int installmentNumber, decimal vehiclePrice, int loanTermMonths, decimal deposit)
        {
            //Get the total amount payable
            decimal totalLoan = (vehiclePrice - deposit) + _fees.AgreementFee + _fees.CompletionFee;
            //Get installment amount
            decimal installmentAmount = (vehiclePrice - deposit) / loanTermMonths;
            //subtract (installemnts * installment amount) from total payable
            decimal amountRemaining = totalLoan - (installmentNumber * installmentAmount);

            if(installmentNumber >=1)
            {
                amountRemaining -= _fees.AgreementFee;
            }
            if(installmentNumber >= loanTermMonths)
            {
                amountRemaining -= _fees.CompletionFee;
            }

            return Math.Round(amountRemaining, 2, MidpointRounding.AwayFromZero);
        }

        /// <summary>
        /// Add agreement fee
        /// </summary>
        /// <param name="installments"></param>
        private void AddAgreementFee(List<KeyValuePair<DateTime, Installment>> installments)
        {
            decimal agreementFee = this._fees.AgreementFee > 0 ? this._fees.AgreementFee : 88;
            KeyValuePair<DateTime, Installment> firstPayment = installments.First();
            installments[0] = new KeyValuePair<DateTime, Installment>(firstPayment.Key,
                new Installment { InstallmentAmount = Math.Round(_installmentAmount + agreementFee, 2), AmountOutstanding = firstPayment.Value.AmountOutstanding });
        }

        /// <summary>
        /// Add completion fee
        /// </summary>
        /// <param name="installments"></param>
        private void AddCompletionFee(List<KeyValuePair<DateTime, Installment>> installments)
        {
            decimal completionFee = this._fees.CompletionFee > 0 ? this._fees.CompletionFee : 20;
            KeyValuePair<DateTime, Installment> lastPayment = installments.Last();
            installments[installments.Count - 1] = new KeyValuePair<DateTime, Installment>(lastPayment.Key,
                new Installment { InstallmentAmount = Math.Round(_installmentAmount + completionFee, 2), AmountOutstanding = lastPayment.Value.AmountOutstanding });
        }
    }
}
